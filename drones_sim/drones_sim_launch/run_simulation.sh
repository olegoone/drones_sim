#!/bin/bash

#
# Source px4 env
#

source ~/ros_workspaces/devel/setup.bash

PX4_DIR=$(source ~/ros_workspaces/devel/setup.bash && rospack find px4)

source ${PX4_DIR}/Tools/setup_gazebo.bash ${PX4_DIR} ${PX4_DIR}/build/px4_sitl_default
export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:${PX4_DIR}:${PX4_DIR}/Tools/sitl_gazebo


roscd drones_sim_models

DRONES_SIM_MODELS_DIR=$(pwd)

export GAZEBO_PLUGIN_PATH=$GAZEBO_PLUGIN_PATH:${DRONES_SIM_MODELS_DIR}/../../../../devel
export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:${DRONES_SIM_MODELS_DIR}/models

echo -e "GAZEBO_PLUGIN_PATH $GAZEBO_PLUGIN_PATH"
echo -e "GAZEBO_MODEL_PATH $GAZEBO_MODEL_PATH"


#
# Launch simulation server
#
roslaunch drones_sim_launch drones_sim.launch &

sleep 5s
rostopic pub /uav0/safety/cmd/demo/goto_goal std_msgs/Empty {} -1
# sleep 1s
# rostopic pub /uav0/safety/cmd/demo/goto_goal std_msgs/Empty {} -1


# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
        
    echo "Killing..."

    # TODO:
    ps aux | grep -i melodic | awk '{print $2}' | xargs kill -9
    ps aux | grep -i ros_workspaces | awk '{print $2}' | xargs kill -9

    exit
}

any_key='a'
until [ $any_key == 'c' -o $any_key == 'q' -o $any_key == 'Q' ]; do
   read any_key   
done