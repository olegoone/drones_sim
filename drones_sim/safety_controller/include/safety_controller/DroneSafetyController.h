/*
 * Filename: DroneSafetyController.h
 *   Author: Oleg Maksimov
 *     Date: June 1, 2020
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2017
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef SRC_SAFETY_CONTROLLER_DRONESAFETYCONTROLLER_H_
#define SRC_SAFETY_CONTROLLER_DRONESAFETYCONTROLLER_H_


#include <functional>
#include <limits>

#include <ros/ros.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <angles/angles.h>

#include <geodesy/utm.h>
#include <std_msgs/Bool.h>
#include <std_msgs/String.h>
#include <geometry_msgs/PoseStamped.h>
#include <sensor_msgs/NavSatFix.h>
#include <mavros_msgs/State.h>
#include <mavros_msgs/CommandBool.h>
#include <mavros_msgs/HomePosition.h>
#include <mavros_msgs/CommandTOL.h>
#include <mavros_msgs/SetMode.h>
#include <mavros_msgs/ExtendedState.h>
#include <rosgraph_msgs/Clock.h>

#include <safety_controller/DronePath.h>


namespace safety {


using namespace std;


/**
 * Safety controller
 */
class DroneSafetyController {

private:

	static const double MIN_TAKEOFF_ALTITUDE;

	/**
	 * Mavros offboard mode name
	 */
	const std::string MODE_OFFBOARD = "OFFBOARD";

public:

	DroneSafetyController(ros::NodeHandle node);

	virtual ~DroneSafetyController();

public:

	inline bool isEnabled() const {
		return isEnabled_;
	}

	inline void setEnabled(bool active) {
		isEnabled_ = active;
	}

	// inline bool isLanded() const {
	// 	return mavrosExtendedState_.landed_state ==
	// 			mavros_msgs::ExtendedState::LANDED_STATE_ON_GROUND;
	// }

	// inline bool isMavlinkConnected() const {
	// 	return mavrosState_.connected;
	// }

	// inline bool isOffboardOn() const {
	// 	return mavrosState_.mode == MODE_OFFBOARD;
	// }

	bool arm();

	bool setMode(const string& mode);

	bool takeoff(const double& altitude = MIN_TAKEOFF_ALTITUDE);

private:

	void publishGoal(geometry_msgs::PoseStamped& goal);

	void armCallback(const std_msgs::Empty);

	void setModeCallback(const std_msgs::String);
	
	void demoGotoGoalCallback(const std_msgs::Empty);

	void takeoffCallback(const std_msgs::Empty);

	void setpointLocalCallback(const geometry_msgs::PoseStamped::ConstPtr& goal);

	void setDronePath(const safety_controller::DronePath& msg);
	
	void setpointGlobalCallback(const sensor_msgs::NavSatFix& navSatMsg);

	void stateCallback(const mavros_msgs::State::ConstPtr& state);

	void localPoseCallback(const geometry_msgs::PoseStamped::ConstPtr& pose);

	void homePositionCallback(const mavros_msgs::HomePosition& msg);


	/**
	 * Called by publish timer at constant rate 
	 */
	void publishGoalTimerCallback(const ros::TimerEvent&);

    // void checkOrientationLock(geometry_msgs::PoseStamped& goal);


	tf::Vector3 getGoalOffset(const geometry_msgs::PoseStamped& goal) const;

	void onGoalReached();

	void onWaypointReached(const int& waypointIndex);

private:

    /**
     * Local pose must be received
     */
    bool localPositionReceived_ = false;

	bool firstPositionReceived_ = false;

	bool onGoalReachedNotified_ = true;

	bool onWaypointReachedNotified_ = true;

	bool isArmed_ = false;

	bool isOffboardState_ = false;

	/**
	 * Is goal publishing enabled
	 */
	bool isEnabled_;

	/**
	 * The locked altitude the drone must preserve
	 */
	double altitudetLock_;

	/**
	 * Indicates that altitude is locked at the same
	 * value it was when offboard mode was enabled,
	 * otherwise, current altitude will be the limit
	 */
	bool altitudeLockEnabled_;

	/**
	 * Indicates that heading value is locked
	 * in goal messages
	 */
	bool headingLockEnabled_;

	/**
	 * Locked heading angle
	 */
	double headingLock_;

	/**
	 * Is connected to ground station
	 */
	// bool gcsConnection_;

	/**
	 * Flight controller state
	 */
	mavros_msgs::State mavrosState_;

	/**
	 * Position of the drone
	 */
	geometry_msgs::PoseStamped localPose_;

	/**
	 * Flight controller extended state (for landed_state)
	 */
	mavros_msgs::ExtendedState mavrosExtendedState_;

	/**
	 * Arming service client
	 */
	ros::ServiceClient setArmingStateService_;

	/**
	 * Takeoff service client
	 */
	ros::ServiceClient takeoffService_;

	/**
	 * SetMode service client
	 */
	ros::ServiceClient setModeService_;

	/**
	 * Land service client
	 */
	ros::ServiceClient landService_;

	/**
	 * Goal publisher
	 */
	ros::Publisher setpointPublisher_;

	ros::Publisher onGoalReachedPublisher_;

	ros::Publisher onWaypointReachedPublisher_;

    /**
     * Local clock publisher
     */
    // ros::Publisher localTimePublisher_;

	ros::Subscriber armCmdSubscriber_;

	ros::Subscriber setModeCmdSubscriber_;

	ros::Subscriber takeoffCmdSubscriber_;

	/**
	 * Setpoint subscriber
	 */
	
	ros::Subscriber setpointLocalSubscriber_;
	
	ros::Subscriber setpointGlobalSubscriber_;
	
	ros::Subscriber homePositionSubscriber_;

	/**
	 * Mavros state subscriber
	 */
	ros::Subscriber stateSubscriber_;

	/**
	 * Mavros extended state subscriber (for landed state)
	 */
	ros::Subscriber extendedStateSubscriber_;

	/**
	 * Local position subscriber for height limit
	 */
	ros::Subscriber localPoseSubscriber_;

	ros::Subscriber demoGotoGoalCmdSubscriber_;

	/**
	 * Connection status to ground station
	 */
	// ros::Subscriber gcsConnectionSubscriber_;

    /**
     * GCS time subscriber
     */
    // ros::Subscriber gcsTimeSubscriber_;

	/**
	 * Goal publisher timer
	 */
	ros::Timer publishTimer_;

	/**
	 * Last received goal
	 */
	geometry_msgs::PoseStamped latestGoal_;

	vector<geometry_msgs::PoseStamped> waypoints_;

	double waypointWaitSeconds_ = 3.0;
	
	int lastGoalArrivalTime_ = 0.0;
	
	int currentTime_ = 0.0;

	tf::Vector3 homePositionUTM_;

	/**
	 * Goal is set
	 */
	bool goalReceived_ = false;

	bool isPathCyclic_ = false;

	std::string localFrame_;

	tf::TransformListener tfListener_;

    geometry_msgs::PoseStamped lastValidGoal_;

    bool orientationLockEnabled_ = false;

    geometry_msgs::Quaternion orientationLock_;

    /**
     * Lock orientation if the goal is closer than this valud
     */
    double orientationLockDistance_ = 2.0;

    double goalTolerance_ = 0.3;
	
    double minAllowedHeight_ = 2.5;
	
    double maxAllowedHeight_ = 50.0;
};


} /* namespace safety */


#endif /* SRC_SAFETY_CONTROLLER_DRONESAFETYCONTROLLER_H_ */
