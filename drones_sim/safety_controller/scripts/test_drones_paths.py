#!/usr/bin/env python

import sys
import time

import rospy
from std_msgs.msg import String
from std_msgs.msg import Empty
from geometry_msgs.msg import PoseStamped
# import safety_controller.DronePath
from safety_controller.msg import DronePath


def addPointToPath(pathArr, x, y, z):

    goal = PoseStamped()

    goal.header.seq = 1
    goal.header.stamp = rospy.Time.now()
    goal.header.frame_id = "map" #TODO

    goal.pose.position.x = x
    goal.pose.position.y = y
    goal.pose.position.z = z

    goal.pose.orientation.x = 0.0
    goal.pose.orientation.y = 0.0
    goal.pose.orientation.z = 0.0
    goal.pose.orientation.w = 1.0

    pathArr.append(goal)



rospy.init_node('test_drones_paths')

# droneId = 0

# armPub = rospy.Publisher('/uav'+str(droneId)+'/safety/cmd/arm', Empty, queue_size=10)
# offboardModePub = rospy.Publisher('/uav'+str(droneId)+'/safety/cmd/set_mode', String, queue_size=10)
setPath0Pub = rospy.Publisher('/uav0/safety/set_path', DronePath, queue_size=10)
setPath1Pub = rospy.Publisher('/uav1/safety/set_path', DronePath, queue_size=10)
setPath2Pub = rospy.Publisher('/uav2/safety/set_path', DronePath, queue_size=10)
setPath3Pub = rospy.Publisher('/uav3/safety/set_path', DronePath, queue_size=10)

drone0PathMsg = DronePath()
drone0PathMsg.waypoints = []
addPointToPath(drone0PathMsg.waypoints, 3,3,3)
addPointToPath(drone0PathMsg.waypoints, 3,3,6)
addPointToPath(drone0PathMsg.waypoints, 3,0,6)
addPointToPath(drone0PathMsg.waypoints, 0,0,6)
addPointToPath(drone0PathMsg.waypoints, 0,0,3)
addPointToPath(drone0PathMsg.waypoints, 0,3,3)
drone0PathMsg.waypointWaitSeconds = 3.0
drone0PathMsg.isPathCyclic = False




drone1PathMsg = DronePath()
drone1PathMsg.waypoints = []
addPointToPath(drone1PathMsg.waypoints, 0,3,3)
addPointToPath(drone1PathMsg.waypoints, 3,3,3)
addPointToPath(drone1PathMsg.waypoints, 3,3,6)
addPointToPath(drone1PathMsg.waypoints, 3,0,6)
addPointToPath(drone1PathMsg.waypoints, 0,0,6)
addPointToPath(drone1PathMsg.waypoints, 0,0,3)
drone1PathMsg.waypointWaitSeconds = 3.0
drone1PathMsg.isPathCyclic = False



drone2PathMsg = DronePath()
drone2PathMsg.waypoints = []
addPointToPath(drone2PathMsg.waypoints, 0,0,3)
addPointToPath(drone2PathMsg.waypoints, 0,3,3)
addPointToPath(drone2PathMsg.waypoints, 3,3,3)
addPointToPath(drone2PathMsg.waypoints, 3,3,6)
addPointToPath(drone2PathMsg.waypoints, 3,0,6)
addPointToPath(drone2PathMsg.waypoints, 0,0,6)
drone2PathMsg.waypointWaitSeconds = 3.0
drone2PathMsg.isPathCyclic = False



drone3PathMsg = DronePath()
drone3PathMsg.waypoints = []
addPointToPath(drone3PathMsg.waypoints, 0,0,6)
addPointToPath(drone3PathMsg.waypoints, 0,0,3)
addPointToPath(drone3PathMsg.waypoints, 0,3,3)
addPointToPath(drone3PathMsg.waypoints, 3,3,3)
addPointToPath(drone3PathMsg.waypoints, 3,3,6)
addPointToPath(drone3PathMsg.waypoints, 3,0,6)
drone3PathMsg.waypointWaitSeconds = 3.0
drone3PathMsg.isPathCyclic = False


# emptyMessage = Empty()
# time.sleep(2.0)
# armPub.publish(emptyMessage)

# offboardMsg = String()
# offboardMsg.data = 'OFFBOARD'
# time.sleep(2.0)
# offboardModePub.publish(offboardMsg)


time.sleep(2.0)

setPath0Pub.publish(drone0PathMsg)
setPath1Pub.publish(drone1PathMsg)
setPath2Pub.publish(drone2PathMsg)
setPath3Pub.publish(drone3PathMsg)
# setPath2Pub.publish(dronePathMsg)
# setPath3Pub.publish(dronePathMsg)

print 'published'
sys.exit()