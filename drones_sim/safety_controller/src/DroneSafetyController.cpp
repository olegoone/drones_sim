/*
 * Filename: DroneSafetyController.cpp
 *   Author: Oleg Maksimov
 *     Date: June 1, 2020
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2017
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <safety_controller/DroneSafetyController.h>


namespace safety {

const double DroneSafetyController::MIN_TAKEOFF_ALTITUDE = 2.5;

DroneSafetyController::DroneSafetyController(ros::NodeHandle node)
	: goalReceived_(false),
      waypoints_(vector<geometry_msgs::PoseStamped>()),
	//   gcsConnection_(false),
	  altitudetLock_(0),
	  altitudeLockEnabled_(false),
	  headingLock_(0),
	  headingLockEnabled_(false) {

	setEnabled(true);

	double publishRate;

	ros::NodeHandle nodePrivate("~");
    nodePrivate.param<std::string> ("local_frame", localFrame_, "local");

	node.param("publish_rate", publishRate, 10.0);

    node.param("goal_tolerance", goalTolerance_, 1.0);

    node.param("min_height", minAllowedHeight_, 2.5);

    node.param("max_height", maxAllowedHeight_, 50.0);

    node.param("orientation_lock_distance", orientationLockDistance_, 2.0);

    setArmingStateService_ = node.serviceClient<mavros_msgs::CommandBool>(
            "mavros/cmd/arming", false);

    takeoffService_ = node.serviceClient<mavros_msgs::CommandTOL>(
            "mavros/cmd/takeoff", false);

    landService_ = node.serviceClient<mavros_msgs::CommandTOL>(
            "mavros/cmd/land", false);
            
    setModeService_ = node.serviceClient<mavros_msgs::SetMode>(
            "mavros/set_mode", false);

	setpointPublisher_ = node.advertise<geometry_msgs::PoseStamped>(
			"mavros/setpoint_position/local", 1, false);

	onGoalReachedPublisher_ = node.advertise<std_msgs::Empty>(
			"safety/goal_reached", 1, false);

	onWaypointReachedPublisher_ = node.advertise<std_msgs::Empty>(
			"safety/waypoint_reached", 1, false);

    // localTimePublisher_ = node.advertise<rosgraph_msgs::Clock>(
	// 		"local_time", 1, true);

	armCmdSubscriber_ = node.subscribe(
			"safety/cmd/arm", 1,
			&DroneSafetyController::armCallback, this);

	setModeCmdSubscriber_ = node.subscribe(
			"safety/cmd/set_mode", 1,
			&DroneSafetyController::setModeCallback, this);

	demoGotoGoalCmdSubscriber_ = node.subscribe(
			"safety/cmd/demo/goto_goal", 1,
			&DroneSafetyController::demoGotoGoalCallback, this);

	takeoffCmdSubscriber_ = node.subscribe(
			"safety/cmd/takeoff", 1,
			&DroneSafetyController::takeoffCallback, this);

	setpointLocalSubscriber_ = node.subscribe(
			"mavros/setpoint_position/safety/local", 1,
			&DroneSafetyController::setpointLocalCallback, this);

	setpointGlobalSubscriber_ = node.subscribe(
			"mavros/setpoint_position/safety/global", 1,
			&DroneSafetyController::setpointGlobalCallback, this);

	setpointLocalSubscriber_ = node.subscribe(
			"safety/set_path", 1,
			&DroneSafetyController::setDronePath, this);


	homePositionSubscriber_ = node.subscribe(
			"mavros/home_position/home", 1,
			&DroneSafetyController::homePositionCallback, this);

    stateSubscriber_ = node.subscribe(
			"mavros/state", 1,
			&DroneSafetyController::stateCallback, this);

	localPoseSubscriber_ = node.subscribe(
			"mavros/local_position/pose", 1,
			&DroneSafetyController::localPoseCallback, this);

	// gcsConnectionSubscriber_ = node.subscribe(
	// 		"gcs/connection", 1,
	// 		&DroneSafetyController::gcsConnectionCallback, this);

	// gcsTimeSubscriber_ = node.subscribe(
	// 		"/gcs/time", 1,
	// 		&DroneSafetyController::gcsTimeCallback, this);

	publishTimer_ = node.createTimer(ros::Duration(1.0 / publishRate), 
		&DroneSafetyController::publishGoalTimerCallback, this);


    latestGoal_.pose.orientation.w = 1.0;
	localPose_.pose.orientation.w = 1.0;
    orientationLock_.w = 1.0;

    // sleep(3);
    
    // takeoff();
}

DroneSafetyController::~DroneSafetyController() {

}

bool DroneSafetyController::setMode(const string& mode) {

    if (isOffboardState_ && MODE_OFFBOARD == mode) {
        return true;
    }

    mavros_msgs::SetMode setModeSrvMsg;
    
    setModeSrvMsg.request.base_mode = 0;
    setModeSrvMsg.request.custom_mode = mode;

    if (!setModeService_.call(setModeSrvMsg) || !setModeSrvMsg.response.mode_sent) {
        ROS_ERROR("Safety: setMode service call failed");

        return false;
    }

    return true;
}

bool DroneSafetyController::arm() {
    
    if (isArmed_) {
        return true;
    }

    mavros_msgs::CommandBool armingSrvMsg;
    
    armingSrvMsg.request.value = true;

    if (!setArmingStateService_.call(armingSrvMsg) || !armingSrvMsg.response.success) {
        ROS_ERROR("Safety: setting arming state failed");

        return false;
    }

    return true;
}
    

bool DroneSafetyController::takeoff(const double& altitude /* = MIN_TAKEOFF_ALTITUDE*/) {

    // arm();

//TODO: perform lat long calculation

    mavros_msgs::CommandTOL takeoffSrvMsg;
    
    takeoffSrvMsg.request.min_pitch = 0.0;
    takeoffSrvMsg.request.yaw = 0.0;
    takeoffSrvMsg.request.latitude = 0.0;
    takeoffSrvMsg.request.longitude = 0.0;
    takeoffSrvMsg.request.altitude = max(MIN_TAKEOFF_ALTITUDE, altitude);

    if (!takeoffService_.call(takeoffSrvMsg) || !takeoffSrvMsg.response.success) {
        ROS_ERROR("Safety: takeoff service call failed");

        return false;
    }


    return true;
}

void DroneSafetyController::armCallback(const std_msgs::Empty) {
    

	arm();

}   

void DroneSafetyController::setModeCallback(const std_msgs::String msg) {
    

	setMode(msg.data);

}   

void DroneSafetyController::demoGotoGoalCallback(const std_msgs::Empty) {
    

    arm();

	setMode("OFFBOARD");

    // takeoff();

    geometry_msgs::PoseStamped point;

    point.pose.position.x = 2.0;//localPose_.pose.position.x - 1 + rand() % 3;
    point.pose.position.y = 0.0;//localPose_.pose.position.y - 1 + rand() % 3;
    point.pose.position.z = 0.1;
    point.pose.orientation.x = 0;
    point.pose.orientation.y = 0;
    point.pose.orientation.z = 0.1;
    point.pose.orientation.w = 1;

    latestGoal_ = point;
    // setpointLocalCallback(&point);
    latestGoal_.pose.position.z = 0.5;//min(max(minAllowedHeight_, point.pose.position.z), maxAllowedHeight_);

    publishGoal(latestGoal_);

}   

void DroneSafetyController::takeoffCallback(const std_msgs::Empty) {
    

	takeoff();

}

void DroneSafetyController::setpointLocalCallback(
		const geometry_msgs::PoseStamped::ConstPtr& goal) {


	latestGoal_ = *goal;

    //TODO: to filter function
    latestGoal_.pose.position.z = min(max(minAllowedHeight_, latestGoal_.pose.position.z), maxAllowedHeight_);


	goalReceived_ = true;
	// The goal will be published by goal publishing timer
}

void DroneSafetyController::setDronePath(const safety_controller::DronePath& msg) {

    waypoints_ = msg.waypoints;

    cerr << "waypoints_ size = " << waypoints_.size() << endl;

    waypointWaitSeconds_ = msg.waypointWaitSeconds;

    // Immediate start
    lastGoalArrivalTime_ = ros::Time::now().toSec() - waypointWaitSeconds_ - 1;

	goalReceived_ = true;

    arm();

	setMode("OFFBOARD");
}


void DroneSafetyController::homePositionCallback(const mavros_msgs::HomePosition& msg) {

    geographic_msgs::GeoPoint lla;
    geodesy::UTMPoint utm;

    lla.latitude = (double)msg.geo.latitude;
    lla.longitude = (double)msg.geo.longitude;
    lla.altitude = (double)msg.geo.altitude;

    geodesy::fromMsg(lla, utm);
    

    homePositionUTM_ = tf::Vector3(utm.easting - msg.position.x, utm.northing - msg.position.y, 0.0);

}

void DroneSafetyController::setpointGlobalCallback(
        const sensor_msgs::NavSatFix& navSatMsg) {

    //
    // Convert LLA to UTM coordinates in world frame
    //
    geographic_msgs::GeoPoint lla;
    geodesy::UTMPoint utm;

    lla.latitude = (double)navSatMsg.latitude;
    lla.longitude = (double)navSatMsg.longitude;
    lla.altitude = (double)navSatMsg.altitude;

    geodesy::fromMsg(lla, utm);

    tf::Vector3 goalLocalPosition = 
            tf::Vector3(utm.easting  - homePositionUTM_.x(), 
                        utm.northing - homePositionUTM_.y(), 
                        lla.altitude);



	// tf::poseStampedMsgToTF(goalLocalPosition, pointMsg);
	// tf::poseStamp(goalLocalPosition, pointMsg);

	latestGoal_.pose.position.x = goalLocalPosition.x();
	latestGoal_.pose.position.y = goalLocalPosition.y();
	latestGoal_.pose.position.z = goalLocalPosition.z();



    //TODO: to filter function
    latestGoal_.pose.position.z = min(max(minAllowedHeight_, latestGoal_.pose.position.z), maxAllowedHeight_);


	goalReceived_ = true;

	// The goal will be published by goal publishing timer
}

void DroneSafetyController::stateCallback(
		const mavros_msgs::State::ConstPtr& state) {

    isArmed_ = state->armed;

    if (state->mode == MODE_OFFBOARD && !localPositionReceived_) {
        // Goal won't be published
        ROS_ERROR("Safety: offboard mode detected but no local position available");
        return;
    }

	if (mavrosState_.mode != MODE_OFFBOARD && // previous state
	        state->mode == MODE_OFFBOARD) {

	    //
	    // Offboard enabled
	    //

        // cerr << " OFFBOARD enabled" << endl;

        isOffboardState_ = true;    

	} else if (mavrosState_.mode == MODE_OFFBOARD && // previous state
	        state->mode != MODE_OFFBOARD) {

	    //
	    // Offboard disabled
	    //

        // cerr << " OFFBOARD disabled" << endl;


        isOffboardState_ = false;    
	}

	mavrosState_ = *state;
}

void DroneSafetyController::localPoseCallback(
		const geometry_msgs::PoseStamped::ConstPtr& pose) {

    //
    // Check local position validity
    //
    if (!isfinite(pose->pose.position.z)) {
        ROS_ERROR("Safety: local position altitude in nan");
        localPositionReceived_ = false;
        return;
    }

    // if (pose->pose.position.z < 0.1) {
    //     ROS_ERROR("Safety: local position altitude is %f", pose->pose.position.z);
    //     localPositionReceived_ = false;
    //     return;
    // }

    if (!isfinite(pose->pose.position.x) || !isfinite(pose->pose.position.y) ||
            !isfinite(pose->pose.orientation.x) || 
            !isfinite(pose->pose.orientation.y) || 
            !isfinite(pose->pose.orientation.z) ||
            !isfinite(pose->pose.orientation.w)) {
        ROS_ERROR("Safety: local position not valid (%f, %f, %f, %f, %f, %f, %f)",
                pose->pose.position.x, pose->pose.position.y, pose->pose.position.z,
                pose->pose.orientation.x, pose->pose.orientation.y, pose->pose.orientation.z,
                pose->pose.orientation.w);

        localPositionReceived_ = false;
        return;
    }

    try {
        tf::getYaw(pose->pose.orientation);
    } catch (...) {
        ROS_ERROR("Safety: local position yaw not valid");
        localPositionReceived_ = false;
        return;
    }

    if (!firstPositionReceived_) {

        latestGoal_ = localPose_;

        // setpointLocalCallback(&localPose_)
        
        firstPositionReceived_ = true;
    }

    //
    // Local pose is valid
    //

    localPositionReceived_ = true;
    

	localPose_ = *pose;

	// if (!altitudeLockEnabled_) {
	//     altitudetLock_ = pose->pose.position.z;
	// }
}

// void DroneSafetyController::gcsConnectionCallback(
// 		const std_msgs::Bool connectionState) {
// 	gcsConnection_ = connectionState.data;
// }

// void DroneSafetyController::gcsTimeCallback(const rosgraph_msgs::Clock::Ptr& time) {
//     if (fabs((ros::Time::now() - time->clock).toSec()) > 2.0) {

//         //
//         // Sync clocks
//         //

// 		// Disabled
//         // ROS_WARN("Syncing clock!");
//         // int exitCode = system("(sudo service chrony stop; sudo ntpdate 192.168.3.202; sudo service chrony start) &");
//     }
// }

void DroneSafetyController::publishGoal(geometry_msgs::PoseStamped& goal) {
    

    lastValidGoal_ = goal;
    
    //
    // Check goal validity
    //
    if (!isfinite(lastValidGoal_.pose.position.x) || !isfinite(lastValidGoal_.pose.position.y) ||
            !isfinite(lastValidGoal_.pose.orientation.x) || 
            !isfinite(lastValidGoal_.pose.orientation.y) || 
            !isfinite(lastValidGoal_.pose.orientation.z) ||
            !isfinite(lastValidGoal_.pose.orientation.w)) {
        ROS_ERROR("Safety: goal not valid (%f, %f, %f, %f, %f, %f, %f)",
                lastValidGoal_.pose.position.x, lastValidGoal_.pose.position.y, lastValidGoal_.pose.position.z,
                lastValidGoal_.pose.orientation.x, lastValidGoal_.pose.orientation.y, lastValidGoal_.pose.orientation.z,
                lastValidGoal_.pose.orientation.w);
    }

    try {
        tf::getYaw(lastValidGoal_.pose.orientation);
    } catch (...) {
        ROS_ERROR("Safety: goal yaw not valid");
    }

    // 
    // Lock altitude
    //
	// lastValidGoal_.pose.position.z = altitudetLock_;

    //
    // Publish setpoint to px4
    //
	setpointPublisher_.publish(lastValidGoal_);

}

tf::Vector3 DroneSafetyController::getGoalOffset(const geometry_msgs::PoseStamped& goal) const {

    //TODO:
    // tf::poseMsgToTF(latestGoal_)

    tf::Vector3 localPoseVec(localPose_.pose.position.x, localPose_.pose.position.y, localPose_.pose.position.z);
    tf::Vector3 latestGoalVec(goal.pose.position.x, goal.pose.position.y, goal.pose.position.z);

    return latestGoalVec - localPoseVec;
}

void DroneSafetyController::onGoalReached() {

    if (onGoalReachedNotified_) {
        return;
    }

    onGoalReachedNotified_ = true;

    std_msgs::Empty msg;
    onGoalReachedPublisher_.publish(msg);

}

void DroneSafetyController::onWaypointReached(const int& waypointIndex) {

    if (onWaypointReachedNotified_) {
        return;
    }

    onWaypointReachedNotified_ = true;

    std_msgs::Empty msg;
    onWaypointReachedPublisher_.publish(msg);

}

void DroneSafetyController::publishGoalTimerCallback(const ros::TimerEvent&) {

    if (!firstPositionReceived_) {
        return;
    }

    // if (!goalReceived_) {

    //     latestGoal_ = localPose_;

    //     // latestGoal_.pose.position.x = roundf(100 * localPose_.pose.position.x) / 100.0;
    //     // latestGoal_.pose.position.y = roundf(100 * localPose_.pose.position.y) / 100.0;
    //     // latestGoal_.pose.position.z = roundf(100 * localPose_.pose.position.z) / 100.0;
    //     // latestGoal_.pose.orientation.x = localPose_.pose.orientation.x;
    //     // latestGoal_.pose.orientation.y = localPose_.pose.orientation.y;
    //     // latestGoal_.pose.orientation.z = localPose_.pose.orientation.z;
    //     // latestGoal_.pose.orientation.w = localPose_.pose.orientation.w;
    // }


    if (!isOffboardState_) {

        latestGoal_ = localPose_;

        // cerr << "Not in OFFBOARD mode, skipping the goal" << endl;
    }
    else {

        tf::Vector3 offset = getGoalOffset(latestGoal_);

        if (offset.length() <= goalTolerance_) {

            onWaypointReached(waypoints_.size());

            if (waypoints_.empty()) {
                onGoalReached();

                latestGoal_ = localPose_;
            }
            else {
                
                // cerr << " Arrived to current goal. Waiting " << waypointWaitSeconds_ << " seconds before going to next waypoint. " << endl;

                // Fetching next waypoint

                if ((ros::Time::now().toSec() - lastGoalArrivalTime_) >= waypointWaitSeconds_) {

                    latestGoal_ = waypoints_.back();

                    cerr << " Going to waypoint: " << latestGoal_.pose.position.x << " ; "  << latestGoal_.pose.position.y << " ; "  << latestGoal_.pose.position.z << endl;

                    if (!isPathCyclic_) {//TODO:

                        waypoints_.pop_back();
                    }
                }
            }
        }
        else {

            lastGoalArrivalTime_ = ros::Time::now().toSec();

            onGoalReachedNotified_ = false;
            onWaypointReachedNotified_ = false;

            // double yaw = atan2(offset.y(), offset.x());
            // latestGoal_.pose.orientation = tf::createQuaternionMsgFromYaw(yaw);
        }
    }

    publishGoal(latestGoal_);
}

} /* namespace safety */

